CC=gcc
LDLIBS=-lm

main: main.o Client.o
	$(CC) $(CFLAGS)-o main main.o Client.o $(LIBS)

main.o: main.c moduly.h
	$(CC) $(CFLAGS)-c main.c

Client.o: Client.c
	$(CC) $(CFLAGS)-c Client.c
