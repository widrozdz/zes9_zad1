#include <stdio.h>
#include <stdlib.h>
#include "moduly.h"

void add_Client(struct Client **head);

int main ()
    {
        struct Client *head;
        head = (struct Client*)malloc(sizeof(struct Client));
        head = NULL;
        char surname[50]="";
        int choice=0;
        printf("\tWelcome\n");
        do
        {
        printf("Please enter the number of the operation you'd like to do: \n");
        printf("1. Add Client/s\n2. Delete Client surname\n3. Display the last names of Clients. \n 4.Display the number of Clients\n5. Exit the program\n___________\n");
        scanf("%d",&choice);

     switch(choice)
            {
            case 1:
                add_Client(&head);
                break;
            case 2:
                printf("Enter the surname of client you'd like to delete:\n");
                scanf("%s",surname);
                pop_by_surname(&head,surname);
                printf("___________\n");
                break;
            case 3:
                show_list(head);
                printf("___________\n");
                break;
            case 4:
                list_size(head);
                printf("___________\n");
                break;
            case 5:
                exit(0);
             default:
                printf("error, invalid option");
            }
        }while (choice!=5);

        return 0;
    }
    void add_Client(struct Client **head)
    {
        char surname[50]="";
        int n=0;
        int i=1;
        printf("How many clients wish you to add (1 or more)?\n");
        scanf("%d", &n);
        do
        {
           printf("Client number %d\n",i);
           scanf("%s", surname);

           push_front(head,surname);
           i++;

        }while(i<=n);
        printf("___________\n");
        return;
    }

